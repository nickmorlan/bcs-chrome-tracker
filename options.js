/*
configuration file
namespace BCSProjects

name ~ display name
user_name ~ login info
password ~ login info
api_projects ~ limit active projects to this array of project ids ex [1,3,6]
api_endpoint ~ endpoint url of api
xver ~ not currently used

*/

BCSProjects = {};

BCSProjects.settings = {
  name:'Nick Morlan',
  user_name:'nick',
  password:'nick',
  api_projects:[],
  api_endpoint:'http://projects.blackcreeksolutions.com/api/',
  xver:'0.1.1'
};

