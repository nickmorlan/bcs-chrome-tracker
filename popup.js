window.addEventListener('load', function() {
  
  // Our default error handler.
  window.onError = function(response) {alert('ff');
    showError(response.errors[0].message);
  };

  // Ah, the joys of asynchronous programming.
  // To initialize, we've got to gather various bits of information.
  // Starting with a reference to the window and tab that were active when
  // the popup was opened ...
  chrome.windows.getCurrent(function(w) {
    chrome.tabs.query({
      active: true,
      windowId: w.id
    }, function(tabs) {
      // Now load our projects based on the settings ...
      $.post(BCSProjects.settings.api_endpoint, JSON.stringify(BCSProjects.settings), function(data) {
        if(data) {console.log(data);
          if(data.status == 'success') {
            BCSProjects.projects = data.projects;
            BCSProjects.tasks = data.tasks;
            showView('add');
            $('#user_name').html("<strong>" + BCSProjects.settings.name + "</strong>");
            BCSProjects.projects.forEach(function(project) {
              var job_code = '';
              if(project.job_code != '') job_code = "(" + project.job_code + ") ";
              $("#project_id").append(
                "<option value='" + project.id + "'>" + job_code + project.name + "</option>");
            });
            $("#project_id").focus();
            $("#project_id").select();
          } else {
            showError(data.timelog);
          }
        } else {
          showError("Cannot connect to server, check settings in options.js file.");
        }
      });
    });
  });
});


// Helper to show a named view.
var showView = function(name) {
  ["login", "add", "success"].forEach(function(view_name) {
    $("#" + view_name + "_view").css("display", view_name === name ? "" : "none");
  });
};

// Show the add UI
var showAddUi = function(url, title, selected_text, options) {
  var self = this;
  showView("add");    
};


// Enable/disable the add button.
var setAddEnabled = function(enabled) {
  var button = $("#add_button");
  if (enabled) {
    if(button.hasClass('disabled')) {
      button.removeClass("disabled");
      button.addClass("enabled");
      button.click(function() {
        createTask();
        return false;
      });
    }
    
  } else {
    button.removeClass("enabled");
    button.addClass("disabled");
    button.unbind('click');
  }
};

// Set the add button as being "working", waiting for the Asana request
// to complete.
var setAddWorking = function(working) {
  setAddEnabled(!working);
  $("#add_button").find(".button-text").text(
      working ? "Saving ..." : "Save Timelog");
};



var createTask = function() {
  console.info("Creating timelog");
  hideError();
  setAddWorking(true);
  BCSProjects.timelog = {
    user_name: BCSProjects.settings.user_name,
    password: BCSProjects.settings.password,
    project_id: $('#project_id').val(),
    task_id: $('#task_id').val(),
    date_logged: $('#date_logged').val(),
    time_logged: $('#time_logged').val(),
    comments: $('#comments ').val()
  };
  console.log(BCSProjects.timelog);
  $.post(BCSProjects.settings.api_endpoint + 'create_timelog', JSON.stringify(BCSProjects.timelog),
          function(response) {
            setAddWorking(false);
            if(response.status == 'success') {
              showSuccess(response.timelog);
            } else {
              showError(response.message);
            }
          }
  );
};

var showError = function(message) {
  console.log("Error: " + message);
  $("#error").css("display", "");
  $("#error_message").text(message);
};

var hideError = function() {
  $("#error").css("display", "none");
};

// Helper to show a success message after a task is added.
var showSuccess = function(timelog) {console.log(timelog);
    $('#s_time_logged').text(timelog.time_logged);
    $('#s_task').text(timelog.task);
    $('#s_project').text(timelog.project);
    $('#s_date_logged').text(timelog.date_logged);
    $('#s_comments').text(timelog.comments);
    $('#new_button').on('click', function() {
        showView("add");
        return false;
      });
    showView("success");
};



// Close the popup if the ESCAPE key is pressed.
window.addEventListener("keydown", function(e) {
  if (e.keyCode === 27) {
    window.close();
  }
}, /*capture=*/false);

$("#close-banner").click(function() { window.close(); });
$("#date_logged").datepicker({
  showOn: 'button',
  buttonImage: "calendarbuttonicon.png",
  showAnim: 'blind'
});
$("#project_id").on('change', function() {
   $('#task_id').find('option').remove();
   var pid = 'pid_' + $(this).val();
    BCSProjects.tasks[pid].forEach(function(task){
      $('#task_id').append("<option value='" + task.id + "'>" + task.name + "</option>");
    });
});

$('.fieldcheck').on('input', function() {
  var okay = true;
  $('.fieldcheck').each(function() {
    if($(this).val() == '') okay = false;
  });
  if(okay) setAddEnabled(true);
})
