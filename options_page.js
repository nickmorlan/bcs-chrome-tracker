
var init = function() {
  $("#reset_button").click(resetOptions);
};



var onChange = function() {
  setSaveEnabled(true);
};

var setSaveEnabled = function(enabled) {
  var button = $("#save_button");
  if (enabled) {
    button.removeClass("disabled");
    button.addClass("enabled");
    button.click(saveOptions);
  } else {
    button.removeClass("enabled");
    button.addClass("disabled");
    button.unbind('click');
  }
};

var resetOptions = function() {
  setSaveEnabled(false);
};

